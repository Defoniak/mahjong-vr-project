Mahjong Riichi en réalité virtuelle pour le Gear VR. Dans l’état actuel, le jeu est uniquement jouable en solo mais il dispose d’une intelligence artificielle à niveau variable pour les adversaires. Il a été réalisé avec Unity et les frameworks d’Oculus. (travail de fin d’étude EPHEC 2016)

Pour des raisons de confidentialité, je ne peux diffuser l’entièreté du code. Les images suivantes sont issues de la présentation de notre TFE.

![be.kikoolab.mahjongriichivr-20160528-114831.jpg](https://bitbucket.org/repo/zLgGxp/images/1893816682-be.kikoolab.mahjongriichivr-20160528-114831.jpg)
![CodeIA.png](https://bitbucket.org/repo/zLgGxp/images/6960680-CodeIA.png)